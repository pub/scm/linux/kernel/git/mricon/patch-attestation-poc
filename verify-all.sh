#!/bin/bash
# This assumes you already have the venv enabled
# If not:
#  python3 -mvenv .venv
#  source .venv/bin/activate
#  pip install -r requirements.txt
#
# Copyright (C) 2020 by the Linux Foundation
# SPDX-License-Identifier: MIT-0
#
echo "--------- UNHASHED (FAIL) ---------"
./main.py -m emails/dev-unsigned.eml verify
echo
echo "--------- ED25519-SIGNED by dev (PASS) ---------"
./main.py -m emails/dev-signed.eml verify
echo
echo "--------- ED25519-SIGNED by rev (PASS) ---------"
./main.py -m emails/rev-signed.eml verify
echo
echo "--------- PGP-SIGNED by mricon (PASS) ---------"
./main.py -m emails/mricon-signed.eml verify
echo
echo "--------- ED25519-SIGNED by dev (PASS, with WARNINGS) ---------"
./main.py -m emails/dev-signed-with-ml-junk.eml verify
echo
echo "--------- ED25519-SIGNED by dev (FAIL) ---------"
./main.py -m emails/dev-signed-invalid.eml verify
echo
